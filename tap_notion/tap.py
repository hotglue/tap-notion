from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_notion.streams import BlocksSteam, SearchPagesStream

STREAM_TYPES = [SearchPagesStream, BlocksSteam]


class TapNotion(Tap):

    name = "tap-notion"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == "__main__":
    TapNotion.cli()